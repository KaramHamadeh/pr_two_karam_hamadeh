namespace PRTWO
{
    /// <summary>
    /// Enum for the state of the Player
    ///
    /// Has three values
    /// </summary>
    public enum PlayerState
    {
        NONE
        IDLE
        ATTACK
        
    }
}